class DuelystConfig:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///duelystdb.db'

class DevConfig(DuelystConfig):
    DEBUG = True
    pass

class TestConfig(DuelystConfig):
    pass

class CiConfig(DuelystConfig):
    pass

CONFIGS = {'dev': DevConfig,
           'test': TestConfig,
           'ci': CiConfig,
           'default': DevConfig}