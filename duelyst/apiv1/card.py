from flask import Blueprint, jsonify

from duelyst.models.card import Card

card_apiv1 = Blueprint("apiv1", __name__, url_prefix='/api/v1/card')

@card_apiv1.route('/', methods=['GET'])
def cards():
    return jsonify(
        results=[card.to_dict() for card in Card.query.limit(10).all()])