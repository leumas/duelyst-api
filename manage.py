import json

from flask_script import Manager, Server
from duelyst.app import create_app
from duelyst.models.db import db
from duelyst.models.card import Card

manny = Manager(create_app)
manny.add_command("runserver", Server())

@manny.command
def load_db():
    db.create_all()
    with open('assets/card_dump.json', 'r') as f:
        cards = json.loads(f.read()).get('cards')

    for card in cards.values():
        new_card = Card.create_from_dump(card)
        db.session.add(new_card)

    db.session.commit()

if __name__ == "__main__":
    manny.run()