import os

from flask import Flask
from duelyst.models.db import db
from duelyst.apiv1.card import card_apiv1

from config import CONFIGS


def create_app(config="default"):
    """ Create an application
    Args:
        config:

    Returns:

    """

    app = Flask(__name__)
    app.config.from_object(CONFIGS.get(os.environ.get('DUELYST_API_CONF') or config))

    db.init_app(app)

    app.register_blueprint(card_apiv1)

    return app

