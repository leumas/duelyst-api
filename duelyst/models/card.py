import datetime

from duelyst.models.db import db


class Card(db.Model):
    """ Represents a single Duelyst Card """
    id = db.Column(db.Integer,
                   primary_key=True,
                   nullable=False,
                   autoincrement=True)

    attack = db.Column(db.Integer, nullable=True)
    category = db.Column(db.String, nullable=False)
    description = db.Column(db.String, nullable=False)
    faction = db.Column(db.String, nullable=False)
    faction_id = db.Column(db.Integer, nullable=False)
    faction_slug = db.Column(db.String, nullable=False)
    hp = db.Column(db.Integer, nullable=True)
    duelyst_id = db.Column(db.Integer, unique=True)
    is_general = db.Column(db.Boolean, nullable=False)
    is_hidden = db.Column(db.Boolean, nullable=False)


    #keywords = db.relationship(db.ForeignKey)
    mana = db.Column(db.Integer, nullable=True)
    name = db.Column(db.String, unique=False, nullable=False)
    race = db.Column(db.String, nullable=True)
    rarity = db.Column(db.String, nullable=False)
    rarity_id = db.Column(db.Integer, nullable=False)
    searchable_content = db.Column(db.Text)
    card_type = db.Column(db.String, nullable=False)

    card_image = db.Column(db.String, nullable=True)

    date_added = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now())

    @classmethod
    def create_from_dump(cls, blob):
        card = Card()
        card.attack = blob.get('attack')
        card.category = blob.get('category')
        card.description = blob.get('description')
        card.faction = blob.get('faction')
        card.faction_id = blob.get('factionId')
        card.faction_slug = blob.get('factionSlug')
        card.hp = blob.get('hp')
        card.duelyst_id = blob.get('id')
        card.is_general = blob.get('isGeneral')
        card.is_hidden = blob.get('isHidden')
        card.mana = blob.get('mana')
        card.name = blob.get('name')
        card.race = blob.get('race')
        card.rarity = blob.get('rarity')
        card.rarity_id = blob.get('rarityId')
        card.searchable_content = blob.get('searchableContent')
        card.card_type = blob.get('type')

        return card

    def __repr__(self):
        return "{0}: {1}: {2}".format(self.id, self.name, self.id)

    def to_dict(self):
        return dict(id=self.id,
                    name=self.name,
                    mana=self.mana,
                    faction=self.faction,
                    searchable_text=self.searchable_content,
                    power=self.attack,
                    toughness=self.hp,
                    card_type=self.card_type,
                    card_image=self.card_image)